package com.ust.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.model.Books;
import com.ust.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository repo;

	public List<Books> getAllBooks() {
		List<Books> findAllBooks = (List<Books>) repo.findAll();
		return findAllBooks;
	}

}
