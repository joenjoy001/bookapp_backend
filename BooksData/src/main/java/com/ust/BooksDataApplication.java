package com.ust;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksDataApplication.class, args);
	}

}
