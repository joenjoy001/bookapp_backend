package com.ust.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ust.model.Books;

public interface BookRepository extends JpaRepository<Books, Integer> {

}
