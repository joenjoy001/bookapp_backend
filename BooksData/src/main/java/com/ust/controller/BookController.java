package com.ust.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ust.model.Books;
import com.ust.service.BookService;

@RestController
public class BookController {
	
	@Autowired
	private BookService service;
	
	@GetMapping(path="/books")
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Books> getAllBooks(){
		List<Books> allBooks = service.getAllBooks();
        return allBooks;
	}
	

}
