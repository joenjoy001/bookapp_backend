package com.ust.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Favourite {
	@Id
	private int id;
	private String bookName;
	private String bookAuthor;
	private String userEmail;
	private String bookUrl;
	public Favourite() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Favourite(int id, String bookName, String bookAuthor, String userEmail, String bookUrl) {
		super();
		this.id = id;
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.userEmail = userEmail;
		this.bookUrl = bookUrl;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getBookAuthor() {
		return bookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getBookUrl() {
		return bookUrl;
	}
	public void setBookUrl(String bookUrl) {
		this.bookUrl = bookUrl;
	}
	
	

}
