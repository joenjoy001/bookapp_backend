package com.ust;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FavListApplication {

	public static void main(String[] args) {
		SpringApplication.run(FavListApplication.class, args);
	}

}
