package com.ust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ust.model.Favourite;

public  interface FavRepository extends JpaRepository<Favourite, Integer> {
	public Favourite findByBookName(String bookName);
	
	List<Favourite> findByUserEmail(String userEmail);
	public Favourite findByBookNameAndUserEmail(String bookName,String userEmail);
}
