package com.ust.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.model.Favourite;
import com.ust.repository.FavRepository;

@Service
public class FavService {
	@Autowired
	private FavRepository repo;
	
	public Favourite saveFav(Favourite fav) {
		return repo.save(fav);
	}
	
	public Favourite fetchListByBookName(String bookName) {
		return repo.findByBookName(bookName);
	}
	public Favourite fetchListByBookNameAndUserMail(String bookName,String userEmail) {
		return repo.findByBookNameAndUserEmail(bookName,userEmail);
	}
	public List<Favourite> getAllFavBooks(String userEmail) {
		return repo.findByUserEmail(userEmail);
	}
	
	public void deleteFavBookById(int id) {
		repo.deleteById(id);
	}
}
