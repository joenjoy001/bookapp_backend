package com.ust.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ust.model.Favourite;
import com.ust.service.FavService;

@RestController
public class controller {

	@Autowired
	private FavService service;

	@PostMapping(path = "/addtolist")
	@CrossOrigin(origins = "http://localhost:4200")
	public Favourite addtoList(@RequestBody Favourite fav) throws Exception {
		String tempBookName = fav.getBookName();
		String tempUserEmail = fav.getUserEmail();
		Favourite tempFav = service.fetchListByBookNameAndUserMail(tempBookName, tempUserEmail);
		if (tempFav != null) {
			throw new Exception("This Book is already in your List");
		}
		Favourite favObj = service.saveFav(fav);

		return favObj;

	}

	@GetMapping(path = "/favlist/{userid}")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Favourite> getAllBooks(@PathVariable("userid") String user) { 
		System.out.println(user);
		List<Favourite> allFavBooks =  service.getAllFavBooks(user);
		return allFavBooks;
	}
	
	@DeleteMapping(path = "/favlist/{bookid}")
	@CrossOrigin(origins = "http://localhost:4200")
	public String reomveFavBooks(@PathVariable ("bookid") int bookid) {
		System.out.println(bookid);
		service.deleteFavBookById(bookid);
		return "removed";
	}
}
