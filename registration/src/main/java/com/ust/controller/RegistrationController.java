package com.ust.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import com.ust.model.User;
import com.ust.service.RegistrationService;

@RestController
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;
	
	@PostMapping(path = "/registeruser" )
	@CrossOrigin(origins ="http://localhost:4200") //connect spring with angular
	public User registerUser(@RequestBody User user) throws Exception {
		String tempEmailId = user.getEmailId();
		if (tempEmailId != null && !"".equals(tempEmailId)) {
			User userObj = service.fetchUserByEmailId(tempEmailId);
			if(userObj != null) {
				throw new Exception("User With "+tempEmailId+" is already Exist");
			}
		}
		User userObj = null;
		userObj = service.saveUser(user);
		return userObj;
	}
	
	@PostMapping(path = "/loginuser")
	@CrossOrigin(origins ="http://localhost:4200")
	public User loginUser(@RequestBody User user) throws Exception {
		String tempEmailId = user.getEmailId();
		String tempPassword = user.getPassword();
		
		User userObj = null;
		
		if(tempEmailId != null && tempPassword != null) { //check pass and email not null
			userObj = service.fetchUserByEmailIdAndPssword(tempEmailId, tempPassword);
		}
		if(userObj == null) { //if null
			throw new Exception("Bad Credentials");
		}
		return userObj; // not null return values
	}
}
